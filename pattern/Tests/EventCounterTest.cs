﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using pattern;
using pattern.Controllers;
using System.IO;
using System.Web.Script.Serialization;

namespace pattern.Tests
{
	[TestFixture]
	public class EventCounterTest
	{
		[Test]
		public void GetEventsCount()
		{
			var controller = new HomeController();
			var deviceId = "d1";
			var fileName = "mock_data.txt";
			EventCounter ec = new EventCounter();
			string dir = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).FullName;

			using (var reader = new StreamReader(dir+ "/../Data/" + fileName))
			{
				ec.ParseEvents(deviceId, reader);
			    Assert.AreEqual(2, ec.GetEventCount(deviceId));
			}
           
		}
	}
}
