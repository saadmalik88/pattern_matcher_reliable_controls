﻿using System.Web.Mvc;
using System.IO;
using System.Web.Script.Serialization;

namespace pattern.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetEventsCount(string deviceId, string fileName) {
            var json = "";
            EventCounter ec = new EventCounter();
            using (var reader = new StreamReader(@"Data/" + fileName))
		      {
		         ec.ParseEvents(deviceId, reader);
		          
                json = new JavaScriptSerializer().Serialize(new 
                {
                    deviceId = deviceId, count = ec.GetEventCount(deviceId)
                });
                return Json(json, JsonRequestBehavior.AllowGet);
		      }

        }
    }

}
