﻿using System;
using System.IO;
using System.Collections.Concurrent;
using System.Globalization;

namespace pattern
{
	public class EventCounter : RC.CodingChallenge.IEventCounter
	{
		ConcurrentDictionary<string, int> dictionary =
			new ConcurrentDictionary<string, int>();
		private Object thisLock = new Object();

		public void ParseEvents(string deviceID, StreamReader eventLog)
		{
            lock (thisLock)
            {
                var prevStage = -1; // used to skip heartbeat
                var patternStart = false; // is true only when stage 2 comes directly after 3 and 3 has been running for 5 or more mins
                var patternLine = new String[3]; //first line data of stage 3 to start looking for pattern
                var prevLine = new String[3]; // line data of previous line to compare with next to determine if 2 comes after 3
                var timeSpan = new TimeSpan();

                while (!eventLog.EndOfStream) // read file
                {
                    var line = eventLog.ReadLine();
                    var values = line.Split('\t');
                    var stage = Convert.ToInt32(values[2]);

                    if (stage != prevStage && stage != 1) // if current stage is equal to last then skip, its for ignoring cycles in stages
                    { 
                        if (!patternStart)
                        {
                            if (stage == 3)
                            {
                                patternLine = values;
                            }
                            if (stage == 2 && Convert.ToInt32(prevLine[2]) == 3) // if stage is 2 and previous stage was 3 then move on to check for time duration
                            { // 2 directly after 3
                                DateTime dateTime1 = DateTime.ParseExact(values[1], "HH:mm:ss",
                                            CultureInfo.InvariantCulture);
                                DateTime dateTime2 = DateTime.ParseExact(patternLine[1], "HH:mm:ss",
                                            CultureInfo.InvariantCulture);

                                timeSpan = dateTime1 - dateTime2; 

                                if (timeSpan.TotalMinutes < 5)// if its been running for 5 minuts
								{
                                    patternStart = false;
                                    patternLine = new String[3];
                                }
                                else
                                {
                                    patternStart = true;
                                }
                            }

                        }
                        else if (patternStart && (stage == 0)) // as soon as there is 0 stage after the initial 3 conditions have been met then increment the counter
						{
                            dictionary.AddOrUpdate(deviceID, 1, (id, count) => count + 1);
                            patternStart = false;
                            patternLine = new String[3];
                        }
                    }
                    prevStage = stage;
                    prevLine = values;
                }
            }
		}

		public int GetEventCount(string deviceID)
		{
            lock (thisLock)
            {
                int value;
                return dictionary.TryGetValue(deviceID, out value) ? value : 0; // return count for device
            }

		}
	}
}
